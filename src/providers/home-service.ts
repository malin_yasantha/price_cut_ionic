import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the HomeService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HomeService {
  public Homedata: any;
  public offerData : any;
  public offersData : any;
  public categoryData : any;
  public companyData : any;
  public cmpOffersData : any;
  public productData : any;
  public searchResults : any;
  public singleCompanyData : any;
  public rateResult : any;
  constructor(public http: Http) {
    console.log('Hello HomeService Provider');
  }

  loadLatestOffers() {

    if (this.Homedata) {
      // already loaded data
      return Promise.resolve(this.Homedata);
    }

    // // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      //this.http.get('https://randomuser.me/api/?results=10')
      this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_latest_deals')
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.Homedata = data;
          resolve(this.Homedata);
        });
    });
  }

  loadOffer(offerID){
    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      //this.http.get('https://randomuser.me/api/?results=10')
      this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_discount_by_id/' + offerID)
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.offerData = data;
          resolve(this.offerData);
        });
    });
  }

    loadCategories(){
      if (this.categoryData) {
        // already loaded data
        return Promise.resolve(this.categoryData);
      }
    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      //this.http.get('https://randomuser.me/api/?results=10')
      this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_all_categories')
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.categoryData = data;
          resolve(this.categoryData);
        });
    });
  }

    getCompaniesByCat(categoryID){
    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      //this.http.get('https://randomuser.me/api/?results=10')
      this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_all_companies/' + categoryID)
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.companyData = data;
          resolve(this.companyData);
        });
    });
  }

    getOffersByCompany(companyID){
      // don't have the data yet
      return new Promise(resolve => {
        // We're using Angular HTTP provider to request the data,
        // then on the response, it'll map the JSON data to a parsed JS object.
        // Next, we process the data and resolve the promise with the new data.
        //this.http.get('https://randomuser.me/api/?results=10')
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_discounts_by_company/' + companyID)
          .map(res => res.json())
          .subscribe(data => {
            // we've got back the raw data, now generate the core schedule data
            // and save the data for later reference
            this.offersData = data;
            resolve(this.offersData);
          });
      });
    }

    getOffersByProducts(productID){
      
      // don't have the data yet
      return new Promise(resolve => {
        // We're using Angular HTTP provider to request the data,
        // then on the response, it'll map the JSON data to a parsed JS object.
        // Next, we process the data and resolve the promise with the new data.
        //this.http.get('https://randomuser.me/api/?results=10')
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_discounts_by_product/' + productID)
          .map(res => res.json())
          .subscribe(data => {
            // we've got back the raw data, now generate the core schedule data
            // and save the data for later reference
            this.cmpOffersData = data;
            resolve(this.cmpOffersData);
          });
      });
    }

    getProductByID(productID){
      return new Promise(resolve => {
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_all_products/' + productID)
          .map(res => res.json())
          .subscribe(data => {
            this.productData = data;
            resolve(this.productData);
          });
      });
    }

    getCompanyByID(companyID){
      
      return new Promise(resolve => {
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/get_company_by_id/' + companyID)
          .map(res => res.json())
          .subscribe(data => {
            this.productData = data;
            resolve(this.productData);
          });
      });
    }

     rateOffer(offerID, rating){
      return new Promise(resolve => {
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/rate_offer/' + offerID + '/' + rating)
          .map(res => res.json())
          .subscribe(data => {
            this.rateResult = data;
            resolve(this.rateResult);
          });
      });
    }

    search(key){
      return new Promise(resolve => {
        this.http.get('http://www.pricecut.lk/mobile_api/public/Api/search_products/' + key)
          .map(res => res.json())
          .subscribe(data => {
            this.singleCompanyData = data;
            resolve(this.singleCompanyData);
          });
      });
    }

}
