import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthServiceK provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthServiceK {

  constructor(public http: Http) {
    //console.log('Hello AuthServiceK Provider');
  }

  //login_service
    login(credentials) {
      
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        
        this.http.post('http://www.pricecut.lk/mobile_api/public/Auth/login', JSON.stringify(credentials), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
            console.log(res);
                     
          }, (err) => {
            reject(err);
          });
    });
  }

  //register_service
  register(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post('http://www.pricecut.lk/mobile_api/public/Auth/signup', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
            
          });
    });
  }

}
