import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';


import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';

import { OffersPage } from '../pages/offers/offers';
import { OfferPage } from '../pages/offer/offer';
import { CategoriesPage } from '../pages/categories/categories';
import { CompanyListPage } from '../pages/company-list/company-list';
import { ProductPage } from '../pages/product/product';
import { NetErrorPage } from '../pages/net-error/net-error';
import { LogoutPage } from '../pages/logout/logout';

import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { AuthServiceK } from '../providers/auth-service-k';


@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    OfferPage,
    CategoriesPage,
    CompanyListPage,
    OffersPage,
    NetErrorPage,
    ProductPage,
    LoginPage,
    SignupPage,
    LogoutPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    OfferPage,
    CategoriesPage,
    CompanyListPage,
    OffersPage,
    ProductPage,
    NetErrorPage,
    LoginPage,
    SignupPage,
    LogoutPage
    
  ],
  //providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AuthServiceK]
})
export class AppModule {}
