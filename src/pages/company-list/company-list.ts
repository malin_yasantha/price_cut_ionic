import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomeService } from '../../providers/home-service';
import { OffersPage } from '../../pages/offers/offers';

import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';
/*
  Generated class for the CompanyList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-company-list',
  templateUrl: 'company-list.html',
  providers: [HomeService, Network]
})
export class CompanyListPage {

  public companyData : any ;

  constructor(public navCtrl: NavController, public navParams: NavParams, public homeService: HomeService, public network : Network) {
    if(this.network.type == 'none'){
     this.navCtrl.setRoot(NetErrorPage);
   }
    let categoryID = this.navParams.get('categoryID');
    console.log(categoryID);
    this.homeService.getCompaniesByCat(categoryID)
    .then(data => {
      this.companyData = data;
      console.log(this.companyData);
    });
  }

  loadOffers(companyID){
    let data = {
      companyID: companyID
    };
    this.navCtrl.push(OffersPage, data);
  }
}
