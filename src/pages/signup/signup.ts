import { Component } from '@angular/core';
import { Network } from "@ionic-native/network";
import { NavController, LoadingController, ToastController , NavParams, ActionSheetController} from 'ionic-angular';
import { AuthServiceK } from "../../providers/auth-service-k";
import { AlertController } from 'ionic-angular';

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  providers: [AuthServiceK, Network]
})
export class SignupPage {
  //[x: string]: any;


  loading: any;
  regData = { email:'', password:'', cpassword: '' };
  data: any;
  

  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceK, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {}
  

  //signup function
  doSignup() {
    

    var email = this.regData.email.trim();
    var password = this.regData.password.trim();
    var cpassword=this.regData.cpassword.trim();
    //validation
    if(email == "" || email == null){
      this.presentToast("Email Required");
      return;
    }

    function validateEmail(email) {
      var re = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;
      return re.test(email);
    }
    if(!validateEmail(email))
    {
      this.presentToast("Enter a valid email");
      return;
    }
    if(password == "" || password == null){
      this.presentToast("Password Required");
      return;
    }
    if(password.length<9){
      this.presentToast("Password required at least 8 characters");
      return;
    }
    
    if(password != cpassword){
      this.presentToast("Password and confirm password should be match");
      return;
    }


    var regData = { email, password ,cpassword};
    this.showLoader();
    
    this.authService.register(this.regData).then((result) => 
    {
      this.loading.dismiss();
      
      this.data = result;

      //alert
      if(this.data.status=="failed")
      {
        //alertbox
        let confirm = this.alertCtrl.create({
          title: 'Failed',
          message: this.data.msg,
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('Agree clicked');
              }
            }
          ]
        });
        confirm.present();
      }
      else
      {
        //alertbox
        let confirm = this.alertCtrl.create({
          title: 'Success',
          message: this.data.msg,
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('Agree clicked');
              }
            }
          ]
        });
        confirm.present();
        
      }
      
      
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
