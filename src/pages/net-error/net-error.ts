import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Page1 } from '../../pages/page1/page1';
import { Network } from '@ionic-native/network';

/*
  Generated class for the NetError page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-net-error',
  templateUrl: 'net-error.html',
  providers: [Network]
})
export class NetErrorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public network : Network) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetErrorPage');
  }

  tryAgain(){
    if(!(this.network.type == 'none')){
      this.navCtrl.setRoot(Page1);
    }
  }

}
