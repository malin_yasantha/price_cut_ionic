import { Component } from '@angular/core';

import { NavController, AlertController,  Platform, ViewController  } from 'ionic-angular';

import { Slides } from 'ionic-angular';

import { OfferPage } from '../../pages/offer/offer';

import {HomeService} from '../../providers/home-service';

import { ProductPage } from '../../pages/product/product';

import { OffersPage } from '../../pages/offers/offers';

import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
  providers: [HomeService, Network]
})

export class Page1 {
  public deals: any;
  public currentItems: any;
  slideurls = [];

  public navigator: any;
  public Connection: any;

  constructor(public navCtrl: NavController, public homeService: HomeService, public network : Network, public platform: Platform,public alertCtrl: AlertController, private viewCtrl: ViewController) {
   if(this.network.type == 'none'){
     this.navCtrl.setRoot(NetErrorPage);
   }

    this.viewCtrl.showBackButton(false);
    this.slideurls.push('http://pricecut.lk/dist/images/slider/banner3.png');
    this.slideurls.push('http://pricecut.lk/dist/images/slider/banner2.png');
    this.slideurls.push('http://pricecut.lk/dist/images/slider/banner1.png');

    this.homeService.loadLatestOffers()
    .then(data => {
      this.deals = data;
      console.log(data);
    });
  }

  showOffer(offerID){
    let data = {
      offerID: offerID
    };
    this.navCtrl.push(OfferPage, data);
  }

  //search functions
  getItems(event){
    let key = event.target.value;
    console.log(key);
    if(key != null && key.length >  2 ){
      console.log(key);
      this.homeService.search(key)
      .then(data => {
        this.currentItems = data;
        console.log(this.currentItems);
      });
    }
    else{
      this.currentItems = NaN;
    }
  }

  openItem(iType, iID){
    if(iType == 'product'){
      let data = {
        productID: iID
      };
      this.navCtrl.push(ProductPage, data);
    }
    else if(iType == 'company'){
        let data = {
        companyID: iID
      };
      this.navCtrl.push(OffersPage, data);
    }
  }

}
