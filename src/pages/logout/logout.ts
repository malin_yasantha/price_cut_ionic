import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import {Page1} from '../page1/page1';

/*
  Generated class for the Logout page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html'
})
export class LogoutPage {

  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public events: Events) {
    localStorage.clear();
    this.showLoader();
    this.events.publish("UPDATE_SIDE_MENU", "");
    this.navCtrl.push(Page1);
    this.loading.dismiss();
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Loging you out...'
    });

    this.loading.present();
  }
  

}
