import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {HomeService} from '../../providers/home-service';
import { OfferPage } from '../../pages/offer/offer';
import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';

/*
  Generated class for the Product page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
  providers: [HomeService, Network]
})
export class ProductPage {
  public deals : any
  public product : any
  public randomNumber: number;
  constructor(public navCtrl: NavController, public network : Network, public navParams: NavParams, public homeService: HomeService) {
    if(this.network.type == 'none'){
      this.navCtrl.setRoot(NetErrorPage);
    }
    this.randomNumber = Math.floor(Math.random() * 4) + 1  ;
    let productID = this.navParams.get('productID');
    console.log(productID);
    this.homeService.getProductByID(productID)
    .then(data => {
      this.product = data;
      console.log(this.product);
    });
    this.homeService.getOffersByProducts(productID)
    .then(data => {
      this.deals = data;
      console.log(this.deals);
    });
  }

  showOffer(offerID){
    let data = {
      offerID: offerID
    };
    this.navCtrl.push(OfferPage, data);
  }

}
