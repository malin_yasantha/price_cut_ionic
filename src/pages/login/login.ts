import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, Events} from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { AuthServiceK } from "../../providers/auth-service-k";

import { Network } from '@ionic-native/network';

import {Http, RequestOptions} from '@angular/http';
import {Page1} from '../page1/page1';
import { AlertController } from 'ionic-angular';


//import { LoginPage } from '../login/login';
//import { SignupPage } from '../signup/signup';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthServiceK, Network]
})
export class LoginPage {
  
  //[x: string]: any;


  loading: any;
  loginData = { email:'', password:'' };
  data: any;
 
  
  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public authService: AuthServiceK, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private http: Http, public events: Events) {}
    //login function
  doLogin() 
  {

    var email = this.loginData.email.trim();
    var password = this.loginData.password.trim();
    //validation
    if(email == null || email==""){
      this.presentToast("Email Required");
      return;
    }
    function validateEmail(email) {
    var re = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;
    return re.test(email);
    }
    if(!validateEmail(email))
    {
      this.presentToast("Enter a valid email");
      return;
    }

    if(password == null || password==""){
      this.presentToast("Password Required");
      return;
    }
    
        this.showLoader();

        var loginData = { email, password };

        this.authService.login(this.loginData).then((result) => {
          this.loading.dismiss();
          
          this.data = result;
          //set token
          

          if(this.data.status=="success")
          {
            //alertbox
            
            localStorage.setItem('userid', this.data.user_id);
            this.events.publish("UPDATE_SIDE_MENU", "");
            let confirm = this.alertCtrl.create({
              title: 'Login success',
              message: this.data.msg,
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    console.log('Agree clicked');
                  }
                }
              ]
            });
            confirm.present();
            this.navCtrl.push(Page1);
          }
          else
          {
            //alertbox
            let confirm = this.alertCtrl.create({
              title: 'Login failed',
              message: this.data.msg,
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    console.log('Agree clicked');
                  }
                }
              ]
            });
            confirm.present();
            this.navCtrl.push(LoginPage);
          }
          
        }, (err) => {
          this.loading.dismiss();
          this.presentToast(err);

        });
      }
  

  //registration
   register() {
    this.navCtrl.push(SignupPage);
  }


  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}


  

