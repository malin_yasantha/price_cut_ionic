import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {HomeService} from '../../providers/home-service';
import { ProductPage } from '../../pages/product/product';

import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';
import { LoginPage} from '../login/login';
import { Page1} from '../page1/page1';

@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
  providers: [HomeService, Network]
})
export class OfferPage {
  public offerData : any;
  public randomNumber : number;
  public offerID : number;
  public rateResult : any;
  constructor(public navCtrl: NavController, public network : Network, public navParams: NavParams, public actionSheetCtrl : ActionSheetController, public alertCtrl : AlertController, public homeService: HomeService) {
    if(this.network.type == 'none'){
     this.navCtrl.setRoot(NetErrorPage);
   }
    this.randomNumber = Math.floor(Math.random() * 3) + 1  ;
    this.offerID = this.navParams.get('offerID');
    this.homeService.loadOffer(this.offerID)
    .then(data => {
      setTimeout(()=>{ 
        let result : any ; 
        result = data ;
        if(result.offer.premium_offer == 1 && (localStorage.getItem('userid') == null)){
          //alert
              let confirm = this.alertCtrl.create({
              title: 'This is a premium offer',
              message: 'You have to log to view premium offers',
              buttons: [
                {
                  text: 'Login',
                  handler: () => {
                    this.navCtrl.push(LoginPage);
                  }
                },
                {
                  text: 'Go back',
                  handler: () => {
                    this.navCtrl.pop();
                  }
                }
                  ]
              });
              confirm.present();
        }
        else{
          this.offerData = data;
        }
        
      },2000);
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Actions',
      buttons: [
        {
          text: 'Order Online',
          icon: 'cart',
          handler: () => {
            //this.offerData.offer.cmp_data.website
            this.showAlert('Oops', 'Sorry, This company does not have a website');
          }
        },{
          text: 'Open Apllication',
          icon: 'appstore',
          handler: () => {
            this.showAlert('Oops', 'Sorry, This feature is currently under constructions');
          }
        },{
          text: 'Rate This Offer',
          icon: 'star',
          handler: () => {
            this.showRatePrompt();
            //this.showAlert('Oops', 'Sorry, This feature is currently under constructions');
          }
        },{
          text: 'Cancel',
          icon: 'close-circle',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  showAlert(title, msg){
    let alert = this.alertCtrl.create({
              title: title,
              subTitle: msg,
              buttons: ['OK']
            });
            alert.present();
  }

  viewProduct(productID){
    console.log(productID);
    let data = {
      productID: productID
    };
    this.navCtrl.push(ProductPage, data);
  }

  showRatePrompt() {
    let prompt = this.alertCtrl.create({
      title: "Rate this offer(1-5)",
      inputs: [
        {
          name: 'rating',
          placeholder: 'Rating'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Rate now',
          handler: data => {
            let rating = parseInt(data.rating);
            if(Number.isInteger(rating) && rating > 0 && rating < 6){
              this.rateOffer(this.offerID,rating);
              this.showAlert('Yayy', 'You rate has been posted successfully');
            
            }
            else{
              this.showAlert('Oops', 'Sorry, This rating is not acceptable. Try a value between 1 - 5');
            }
          }
        }
      ]
    });
    prompt.present();
  }

  rateOffer(offerID, rating){
    this.homeService.rateOffer(offerID,rating)
            .then(data => {
              setTimeout(()=>{ 
                this.rateResult = data;
                console.log(this.rateResult);
                if(!this.rateResult.error){
                }
              },3000);
            });
  }
}
