import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomeService } from '../../providers/home-service';
import { OfferPage } from '../../pages/offer/offer';

import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
  providers: [HomeService, Network]
})
export class OffersPage {

  public deals: any;
  public companyData: any;
  public randomNumber: number;

  constructor(public navCtrl: NavController, public network : Network, public navParams: NavParams, public homeService: HomeService) {
    if(this.network.type == 'none'){
      this.navCtrl.setRoot(NetErrorPage);
    }
    this.randomNumber = Math.floor(Math.random() * 4) + 1  ;
    let companyID = this.navParams.get('companyID');
    console.log(companyID);
    this.homeService.getCompanyByID(companyID)
    .then(data => {
      this.companyData = data[0];
      console.log(this.companyData);
    });
    this.homeService.getOffersByCompany(companyID)
    .then(data => {
      this.deals = data;
      console.log(this.deals);
    });
  }

  showOffer(offerID){
    let data = {
      offerID: offerID
    };
    this.navCtrl.push(OfferPage, data);
  }

}
