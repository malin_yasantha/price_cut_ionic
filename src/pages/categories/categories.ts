import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomeService } from '../../providers/home-service';
import { CompanyListPage } from '../../pages/company-list/company-list';
import { NetErrorPage } from '../../pages/net-error/net-error';
import { Network } from '@ionic-native/network';

/*
  Generated class for the Categories page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
  providers: [HomeService, Network]
})
export class CategoriesPage {

  public categoryData : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public homeService: HomeService, public network : Network) {
    if(this.network.type == 'none'){
     this.navCtrl.setRoot(NetErrorPage);
   }
    this.homeService.loadCategories()
    .then(data => {
      this.categoryData = data;
      console.log(this.categoryData);
    });
  }

  showCompanies(categoryID){
    let data = {
      categoryID: categoryID
    };
    this.navCtrl.push(CompanyListPage, data);
  }

}
